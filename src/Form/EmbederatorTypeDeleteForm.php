<?php

namespace Drupal\embederator\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for deleting an embederator_type entity.
 *
 * @ingroup embederator
 */
class EmbederatorTypeDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $count = \Drupal::entityTypeManager()
      ->getStorage('embederator')
      ->getQuery()
      ->condition('type', $this->entity->id())
      ->count()
      ->execute();
    return $this->t('Are you sure you want to delete entity type %name? This will delete %count entities of this type.', [
      '%name' => $this->entity->label(),
      '%count' => $count,
    ]);
  }

  /**
   * {@inheritdoc}
   *
   * If the delete command is canceled, return to the proxy list.
   */
  public function getCancelUrl() {
    return new Url('entity.embederator_type.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   *
   * Delete the entity and log the event. logger() replaces the watchdog.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Count entities prior to deletion.
    $storage = \Drupal::entityTypeManager()
      ->getStorage('embederator');
    $count = $storage
      ->getQuery()
      ->condition('type', $this->entity->id())
      ->count()
      ->execute();
    
    // Delete entities of this type.
    $entities = $storage
      ->loadByProperties(['type' => $this->entity->id()]);
    foreach ($entities as $e) {
      $e->delete();
    }

    // Delete the type.
    $entity = $this->getEntity();
    $entity->delete();

    $this->logger('embederator')->notice('Deleted embederator type %title and removed %count entities.',
      [
        '%title' => $this->entity->label(),
        '%count' => $count,
      ]);
    $form_state->setRedirect('entity.embederator_type.collection');
  }

}
